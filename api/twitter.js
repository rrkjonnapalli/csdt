'use strict';
const router = require('express').Router({ mergeParams: true });
const passport = require('passport');
const con = require('../connectors/mongo');
const twit = require('../connectors/twitter');
const TwitBot = require('../utils/twitBot');
const logger = require('../utils/logger');
const Errors = require('../utils/responses');
setTimeout(() => {
  let twitBot = new TwitBot();
}, 5 * 1000);

router.get('/', passport.authenticate('twitter'));


router.get('/callback', (req, res, next) => {
  passport.authenticate('twitter', (err, user, info) => {
    if (err) throw err;
    try {
      user = JSON.parse(user);
    } catch (error) {

    }
    try {
      info = JSON.parse(info);
    } catch (error) {

    }
    if (req.user && !req.user.tuId) {
      let query = { _id: req.user._id };
      let update = { $set: { tuId: user.id, tuname: user.username, tname: user.displayName } };
      con.User._collection.updateOne(query, update, (err, res) => {
      });
    }
    res.redirect('/settings');
    // res.send({ user, info });
  })(req, res, next);
});

// router.get('/mentions', (req, res, next) => {
//   twit.getMentions((err, data) => {
//     if (err) throw err;
//     res.send(data);
//   });
// });

router.use((req, res, next) => {
  if (req.user && req.user.role === 'admin') return next();
  res.send(Errors.unauthorised);
});

router.get('/mentions', (req, res, next) => {
  let uId = req.query.uid ? req.query.uid.trim() : '';
  if (!uId) { return res.send(Errors.getMessage(0, 'Invalid user id')); }
  con.Tweet.getTweetsByUser(uId, { sort: [['tctdAt', -1]] }, (err, tweets) => {
    if (err) logger.error(err);
    res.send(Errors.getMessage(1, { data: tweets.reverse() }));
  });
});

router.get('/users', (req, res) => {
  con.Tweet.getTweetUsers((err, twtUsers) => {
    if (err) throw err;
    if (!twtUsers) return res.send({ error: true });
    let uids = twtUsers.map(usr => usr._id);
    con.Tuser._collection.find({ _id: { $in: uids } }).toArray((err, users) => {
      if (err) throw err;
      if (!users) return res.send({ error: true });
      res.send({ error: false, data: users });
    });
  });
});

module.exports = router;