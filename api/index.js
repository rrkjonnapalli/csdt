"use strict";
const router = require('express').Router({ mergeParams: true });

const authRouter = require('./auth');
const twitRouter = require('./twitter');
const userRouter = require('./user');
router.use('/auth', authRouter);

router.use('/twitter', twitRouter);

router.use('/user', (req, res, next) => {
  if (req.user) return next();
  next(new Error('Unauthorized.'));
}, userRouter);

module.exports = router;