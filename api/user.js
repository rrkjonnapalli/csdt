'use strict';
const router = require('express').Router({ mergeParams: true });
const con = require('../connectors/mongo');
const logger = require('../utils/logger');
const Errors = require('../utils/responses');
router.get('/', (req, res) => {
  const userId = req.user._id;
  con.User.findById(userId, (err, user) => {
    if (err) {
      logger.error(err);
      return res.send(Errors.somethingWentWrong);
    }
    if (!user) { return res.send(Errors.userNotFound); }
    delete user.pwd;
    res.send(Errors.getMessage(1, { data: user }));
  });
});

module.exports = router;