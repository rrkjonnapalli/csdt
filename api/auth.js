"use strict";
const router = require('express').Router({ mergeParams: true });
const passport = require('passport');
const con = require('../connectors/mongo');
const Errors = require('../utils/responses');

router.post('/', (req, res) => {
  res.send({ error: false });
});

router.all('/check', (req, res) => {
  if (req.user) {
    res.send({ error: false, data: req.user });
  } else {
    res.send({ error: true });
  }
});

router.all('/signout', (req, res) => {
  req.session.destroy();
  req.logout();
  res.send({ error: false });
});

router.use((req, res, next) => {
  if (req.user) return res.send(Errors.methodNotAllowed);
  next();
});

router.post('/signin', (req, res, next) => {
  passport.authenticate('local', { session: true }, (err, user, info) => {
    if (err) throw err;
    if (!user) return res.send(info);
    req.logIn(user, (err) => {
      if (err) throw err;
      res.send(info);
    });
  })(req, res, next);
});

router.post('/signup', (req, res, next) => {
  con.User.signUp(req.body, (err, response) => {
    if (err) throw err;
    res.send(response);
  });
});


module.exports = router;