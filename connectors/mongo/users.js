"use strict";
const collectionName = require('./collections').User;
const Errors = require('../../utils/responses');
const Base = require('./base');
class User extends Base {
  constructor(db, options = {}) {
    options.collectionName = collectionName;
    super(db, options);
    this._db = db;
    this._collection = this._db.collection(collectionName);
  }

  findByEmail(email, projection, cb) {
    if (typeof projection === 'function') {
      cb = projection;
      projection = {};
    }
    this._collection.findOne({ email }, { projection }, cb);
  }
  findByTuId(tuId, projection, cb) {
    if (typeof projection === 'function') {
      cb = projection;
      projection = {};
    }
    this._collection.findOne({ tuId }, { projection }, cb);
  }

  signIn(data, cb) {
    if (!data) return cb(null, Errors.emptyRequest);
    let username = data.username ? data.username.trim().toLowerCase() : undefined;
    let pwd = data.pwd ? data.pwd.trim() : undefined;
    if (!username || !pwd) return cb(null, Errors.missingRequiredFields);
    this.findByEmail(username, (err, data) => {
      if (err) throw err;
      if (!data) return cb(null, Errors.userNotFound);
      if (data.pwd !== pwd) return cb(null, Errors.invalidCredentials);
      data.id = data._id;
      delete data.pwd;
      return cb(null, Errors.getMessage(1, { data }));
    });
  }

  signUp(data, cb) {
    if (!data) return cb(null, Errors.emptyRequest);
    let email = data.email ? data.email.trim().toLowerCase() : undefined;
    let pwd = data.pwd ? data.pwd.trim() : undefined;
    let name = data.name ? data.name.trim() : undefined;
    if (!email || !pwd || !name) return cb(null, Errors.missingRequiredFields);
    this.findByEmail(email, (err, user) => {
      if (err) throw err;
      if (user) return cb(null, Errors.userExists);
      this.create(data, { autoIncrement: true }, (err, nuser) => {
        if (err) throw err;
        return cb(null, Errors.getMessage(1));
      });
    });
  }

}

module.exports = User;