"use strict";
const collectionName = require('./collections').Tweet;
const Errors = require('../../utils/responses');
const Base = require('./base');
class Tweet extends Base {

  constructor(db, options = {}) {
    options.collectionName = collectionName;
    super(db, options);
    this._db = db;
    this._collection = this._db.collection(collectionName);
  }

  getTweetUsers(opts, cb) {
    if (typeof opts === 'function') {
      cb = opts;
      opts = {};
    }
    let limit = opts.limit || 30;
    let dateLimit = new Date().setHours(0, 0, 0, 0) - 30 * 24 * 60 * 60 * 1000;
    dateLimit /= 1000;
    this._collection.aggregate([
      { $match: { tctdAt: { $gt: dateLimit } } },
      { $group: { _id: '$uId' } },
      { $sort: { tctdAt: -1 } },
      { $limit: limit }
    ]).toArray(cb);
  }

  getTweetsByUser(uId, opts, cb) {
    if (typeof opts === 'function') {
      cb = opts;
      opts = {};
    }
    let limit = opts.limit || 25;
    this._collection.find({ $or: [{ uId }, { touId: uId }] }, opts).toArray(cb);
  }

}

module.exports = Tweet;