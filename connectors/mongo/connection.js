"use strict";
const MongoClient = require('mongodb').MongoClient;
const config = require('config');
const logger = require('../../utils/logger');
const User = require('./users');
const Tweet = require('./tweets');
const Tuser = require('./tusers');

class Connection {
  constructor(opts) {
    const conf = opts || config.get('mongo');
    const uri = `mongodb://${conf.host}:${conf.port}`;
    let options = {
      authSource: conf.db,
      useNewUrlParser: true,
      reconnectInterval: 1000,
      reconnectTries: Number.MAX_VALUE,
      keepAlive: true
    };
    if (conf.auth) options.auth = conf.auth;
    MongoClient.connect(uri, options)
      .then(client => {
        this.db = client.db(conf.db);
        logger.info('Mongodb connection established.');
        this.User = new User(this.db);
        this.Tweet = new Tweet(this.db);
        this.Tuser = new Tuser(this.db);
      })
      .catch(err => {
        logger.error(err);
        // if (cb) cb(null, this.db);
      });
  }
}

module.exports = Connection;