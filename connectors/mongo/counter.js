"use strict";
const collectionName = require('./collections').Counter;

class Counter {
  constructor(db, options = {}) {
    this._db = db;
    this.collectionName = options.collectionName;
    this.counterCollection = this._db.collection(collectionName);
  }
  _nextId(cb) {
    let query = { _id: this.collectionName };
    let update = { $inc: { seq: 1 } };
    let options = { returnOriginal: false, upsert: true };
    this.counterCollection.findOneAndUpdate(query, update, options, (err, counter) => {
      if (typeof cb !== 'function') return;
      if (err) return cb(err);
      cb(null, counter.value.seq);
    });
  }
  updateId(colName, id, cb) {
    if (typeof id === 'function') {
      cb = id;
      id = colName;
      colName = this.collectionName;
    }
    let query = { _id: colName };
    let update = { $set: { seq: id } };
    let options = { upsert: true };
    this.counterCollection.updateOne(query, update, options, (err, counter) => {
      if (typeof cb !== 'function') return;
      if (err) return cb(err);
      cb(null, true);
    });
  }
  getId(colName, cb) {
    if (typeof colName === 'function') {
      cb = colName;
      colName = this.collectionName;
    }
    let query = { _id: colName };
    this.counterCollection.findOne(query, (err, counter) => {
      if (typeof cb !== 'function') return;
      if (!counter) return cb(err, null);
      cb(null, counter.seq);
    });
  }
}

module.exports = Counter;