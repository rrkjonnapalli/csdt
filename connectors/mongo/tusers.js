"use strict";
const collectionName = require('./collections').Tuser;
const Errors = require('../../utils/responses');
const Base = require('./base');
class Tuser extends Base {
  constructor(db, options = {}) {
    options.collectionName = collectionName;
    super(db, options);
    this._db = db;
    this._collection = this._db.collection(collectionName);
  }
  findOrCreate(user, cb) {
    this.findById(user.id, (err, ftuser) => {
      if (err || ftuser) return cb(err, ftuser);
      let _id = user.id;
      let tuser = { _id, name: user.name.trim(), uname: user.username.trim() };
      // let query = { _id };
      this.create(tuser, (err, user) => {
        cb(err, tuser);
      });
      // this._collection.updateOne(query, { $set: tuser }, { upsert: true }, (err, user) => {
      //   if (tuser) tuser._id = _id;
      //   cb(err, tuser);
      // });
    });
  }
}

module.exports = Tuser;