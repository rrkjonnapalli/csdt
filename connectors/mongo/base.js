'use strict';
const logger = require('../../utils/logger');
const async = require('async');
const Counter = require('./counter');

class Base extends Counter {

  constructor(db, options = {}) {
    super(db, options);
    this._db = db;
    this._collectionName = options ? options.collectionName : undefined;
  }

  findById(_id, projection, cb) {
    if (typeof projection === 'function') {
      cb = projection;
      projection = {};
    }
    this._db.collection(this._collectionName).findOne({ _id }, { projection }, (err, data) => {
      if (cb) cb(err, data);
    });
  }

  create(data, opts, cb) {
    if (typeof opts === 'function') {
      cb = opts;
      opts = {};
    }
    async.waterfall([
      (callback) => {
        if (!opts.autoIncrement) return callback();
        this._nextId(callback);
      },
      (id, callback) => {
        if (typeof id === 'function') {
          callback = id;
        } else {
          data._id = id;
        }
        this._db.collection(this._collectionName).insertOne(data, callback);
      }
    ], (err, data) => {
      if (cb) cb(err, data);
    });
  }
}
module.exports = Base;