'use strict';
const con = require('./mongo');

let signin = (user, pwd) => {
  con.User.signIn({ username: user, pwd: pwd }, (err, data) => console.log(err, data));
};

let signup = (email, pwd, name) => {
  con.User.signUp({ email, pwd, name }, (err, data) => console.log(err, data));
};

setTimeout(() => {
  // signup('rrk', 'tom', 'ravi');
  // signin('rrks', 'tom');
  con.Tweet.getTweetUsers((err, data) => {
    console.log(err, data);
  })
}, 1000);