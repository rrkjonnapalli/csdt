'use strict';
const twit = require('../../utils/twitUtil');
class Twitter {
  constructor() { }
  getMentions(opts, cb) {
    if (typeof opts === 'function') {
      cb = opts;
      opts = {};
    }
    let uri = `https://api.twitter.com/1.1/statuses/mentions_timeline.json`;
    let options = {};
    if(opts.since_id) options.since_id = opts.since_id;
    twit(uri, options, { method: 'GET' }, cb);
  }

  directMessage(opts, cb) {
    if (typeof opts === 'function') {
      throw new Error('opts cannot be empty.');
    }
    let data = {
      event: {
        type: 'message_create',
        message_create: {
          target: { recipient_id: opts.recipient_id },
          message_data: { text: opts.message }
        }
      }
    };
    let uri = `https://api.twitter.com/1.1/direct_messages/events/new.json`;
    twit(uri, data, { method: 'POST', json: true }, cb);
  }

  updateStatus(opts, cb) {
    let uri = `https://api.twitter.com/1.1/statuses/update.json`;
    twit(uri, { status: opts.status, in_reply_to_status_id: opts.in_reply_to_status_id }, { method: 'POST' }, cb);
  }

  getStatus(opts, cb) {
    let uri = `https://api.twitter.com/1.1/statuses/show.json`;
    twit(uri, { id: opts.id }, { method: 'GET' }, cb);
  }
}

module.exports = Twitter;