'use strict';
const tracer = require('tracer');
const logger = tracer.colorConsole({
  format: "{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})",
  dateformat: "dd-mm-yyyy HH:MM:ss TT"
});

module.exports = logger;