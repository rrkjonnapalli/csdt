'use strict';
class DateTime {
  static getTimestamp(dt, opts = {}) {
    let result = new Date(dt).getTime();
    if (opts.ms) result *= 1000;
    return result / 1000;
  }
  static getDate(ts, opts = {}) {
    ts = Number.isInteger(ts) ? ts : parseInt(ts);
    if (!Number.isInteger(ts)) throw new Error('Invalid timestamp.');
    if (opts.ms) ts *= 1000;
  }
  static getDay(dt, opts = {}) {
    if (typeof dt === 'number' && opts.ms) dt *= 1000;
    let dts = new Date(dt).setHours(0, 0, 0, 0);
    if (opts.date) return new Date(dts);
    return dts / 1000;
  }
}