'use strict';
const asnc = require('async');
const twit = require('../connectors/twitter');
const con = require('../connectors/mongo');
const config = require('config');
const twitCnfg = config.get('twitter');
const logger = require('../utils/logger');
const twitEmitter = require('../utils/twitEmitter');

let tweetHandler = (tweet, cb) => {
  let tctdAt = new Date(tweet.created_at).getTime() / 1000;
  let uId = tweet.user.id_str;
  let twt = {
    _id: tweet.id_str,
    text: tweet.text,
    tctdAt,
    uId
  };
  if (tweet.in_reply_to_status_id_str) twt.rplyTo = tweet.in_reply_to_status_id_str;

  let tuser = { id: tweet.user.id_str, name: tweet.user.name, username: tweet.user.screen_name };
  con.Tuser.findOrCreate(tuser, (uerr, user) => {
    if (uerr) logger.error(uerr);
    twitEmitter.emit('tuser', user);
    con.Tweet.create(twt, (terr, utwt) => {
      if (terr) logger.error(terr);
      cb(null, twt);
    });
  });
};

class TwitBot {
  constructor() {
    this.init();
  }
  init() {
    con.Tweet.getId((err, id) => {
      if (err) logger.error(err);
      this.since_id = id;
      this.recentTime = 0;
      if (!id) this.startBOT();
      else {
        con.Tweet.findById(id, { tctdAt: 1 }, (err, tweet) => {
          if (tweet) this.recentTime = tweet.tctdAt;
          this.startBOT();
        });
      }
    });
  }
  startBOT() {
    asnc.forever(
      (next) => {
        let opts = {};
        if (this.since_id) opts.since_id = this.since_id;
        twit.getMentions(opts, (err, data) => {
          if (err) logger.error(err);
          this.processTweets(data);
          setTimeout(next, twitCnfg.retrivalInterval * 1000);
        });
      },
      (err) => {
        logger.error(err);
      });
  }

  processTweets(data) {
    if (!data || !Array.isArray(data) || data.length === 0) return;
    asnc.eachSeries(data, (tweet, callback) => {
      let tctdAt = new Date(tweet.created_at).getTime() / 1000;
      if (this.recentTime < tctdAt) {
        this.since_id = tweet.id_str;
        this.recentTime = tctdAt;
      }
      tweetHandler(tweet, (err, twt) => {
        twitEmitter.emit('tweet', twt);
        callback();
      });
    }, (err, result) => {
      if (this.since_id)
        con.Tweet.updateId(this.since_id, (err, data) => { });
    });
  }
}

module.exports = TwitBot;