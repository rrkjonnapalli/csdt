"use strict";
const config = require('config');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;
const con = require('../connectors/mongo');

const twitConfig = config.get('twitter');

passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
  con.User.findById(id, { email: 1, name: 1, role: 1 }, (err, user) => {
    cb(err, user);
  });
});

passport.use(new LocalStrategy((username, password, done) => {
  con.User.signIn({ username, pwd: password }, (err, response) => {
    if (err) throw done(err);
    if (response.error) return done(null, false, response);
    done(null, response.data, response);
  });
}));

passport.use(new TwitterStrategy({
  consumerKey: twitConfig.consumerKey,
  consumerSecret: twitConfig.consumerSecret,
  callbackURL: twitConfig.callbackURL,
}, (token, tokenSecret, profile, cb) => {
  // In this example, the user's Twitter profile is supplied as the user
  // record.  In a production-quality application, the Twitter profile should
  // be associated with a user record in the application's database, which
  // allows for account linking and authentication with other identity
  // providers.
  return cb(null, profile);
}));

module.exports = passport;