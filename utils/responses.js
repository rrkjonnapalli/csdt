'use strict';
const TYPES = { 0: 'error', 1: 'success' };
let getMessage = (type, message, opts) => {
  type = TYPES[type];
  let result = {};
  if (typeof message === 'object') { opts = message; message = undefined; }
  if (message) result.message = message;
  Object.assign(result, opts);
  switch (type) {
    case 'error': result.error = true; break;
    case 'success': result.error = false; break;
  }
  return result;
};

module.exports = {
  emptyRequest: getMessage(0, 'Empty request'),
  missingRequiredFields: getMessage(0, 'Missing required fields.'),
  userExists: getMessage(0, 'User already exists.'),
  userNotFound: getMessage(0, 'User not found.'),
  invalidCredentials: getMessage(0, 'Invalid credentials.'),
  methodNotAllowed: getMessage(0, 'Method not allowed.'),
  unauthorised: getMessage(0, 'Unauthorised.'),
  success: getMessage(1),
  somethingWentWrong: getMessage(0, 'Something went wrong.'),
  getMessage
};