'use strict';

let mongoReturnHandler = (err, data, cb) => {
  if (err) throw err;
  if (!data) return cb(err, data);
  if (data & typeof data === 'object') {
    if (Array.isArray(data)) {
      cb(err, data.map(e => { if (e._id) e.id = e._id; return e; }));
    } else {
      data.id = data._id;
      cb(err, data.id);
    }
  } else {
    cb(err, data);
  }
};

module.exports = { mongoReturnHandler };