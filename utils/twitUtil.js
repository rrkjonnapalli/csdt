'use strict';
const request = require('request');
const config = require('config');
const twitConfig = config.get('twitter');
var qs = require("querystring");

module.exports = (url, data, opts, cb) => {
  const tconf = opts.config || twitConfig;
  const method = opts.method;
  if (!opts.json) {
    let query = qs.stringify(data);
    if (query) url += `?${query}`;
    data = undefined;
  }
  const oauth = {
    consumer_key: tconf.consumerKey,
    consumer_secret: tconf.consumerSecret,
    token: tconf.accessToken,
    token_secret: tconf.accessTokenSecret
  };

  let options = { url, method, oauth };
  if (data) options.json = data;
  request(options, (err, response, body) => {
    try { body = JSON.parse(body); } catch (error) { }
    if (cb) cb(err, body, response);
  });
};