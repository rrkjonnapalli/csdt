"use strict";
const config = require('config');
const http = require('http');
const express = require('express');
const session = require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true });
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
let sharedsession = require("express-socket.io-session");
// logger
const logger = require('./utils/logger');

// passport authentication
let passport = require('./utils/passport');

// api
const api = require('./api');
const indexRouter = require('./routes');
// socketio
const socketio = require('./sockets');

app.use(session);
app.use(passport.initialize());
app.use(passport.session());

const LocalStrategy = require('passport-local').Strategy;
const con = require('./connectors/mongo');

// app.use(morgan(':remote-addr - :remote-user [:date[web]] ":method :url HTTP/:http-version" :status :response-time ms - :res[content-length]'));
app.use(morgan('dev'));

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));
app.use(cors());

app.use(express.static('public'));

// api integration
app.use('/api', api);
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, './public/index.html'));
});
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err.message);
});

const host = config.get('host');
const port = process.env.PORT || config.get('port');


/** server begin*/

var server = http.createServer(app);
const io = require('socket.io')(server);
io.use(sharedsession(session, { autoSave: true, resave: true, saveUninitialized: true }));
socketio(io);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, host);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  logger.info('Server listening on port', port);
}

/** server end */
