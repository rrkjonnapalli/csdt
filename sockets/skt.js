'use strict';
const config = require('config');
const twitEmitter = require('../utils/twitEmitter');
const logger = require('../utils/logger');
const con = require('../connectors/mongo');
const twit = require('../connectors/twitter');
let tweetHandler = (tweet, cb) => {
  let tctdAt = new Date(tweet.created_at).getTime() / 1000;
  let uId = tweet.user.id_str;
  let twt = {
    _id: tweet.id_str,
    text: tweet.text,
    tctdAt,
    uId,
    touId: tweet.touId
  };
  if (tweet.in_reply_to_status_id_str) twt.rplyTo = tweet.in_reply_to_status_id_str;

  con.Tweet.create(twt, (terr, utwt) => {
    if (terr) logger.error(terr);
    cb(null, twt);
  });
};

module.exports = (skt, io) => {

  // skt.emit('msg', { msg: 'Hello you are successfully connected.' });
  // skt.on('test', (data) => {
  //   console.log(data);
  // });
  skt.on('replyTweet', (data) => {
    if (!data.id || !data.text) return;
    let tweet = {};
    con.Tweet.findById(data.id, { _id: 1, uId: 1 }, (err, twt) => {
      if (err) logger.error(err);
      if (!twt) return;
      if (data.id) tweet.in_reply_to_status_id = data.id;
      if (data.text) tweet.status = data.text.trim();
      let uId = data.uId || twt.uId;
      con.Tuser.findById(uId, (err, tuser) => {
        if (err) logger.error(err);
        if (err || !tuser) return;
        let username = tuser.uname;
        if (username && !tweet.status.includes(`@${username}`)) {
          tweet.status = `@${username} ${tweet.status}`;
        }
        twit.updateStatus(tweet, (err, newTwt) => {
          if (err) logger.error(err);
          newTwt.touId = uId;
          tweetHandler(newTwt, (err, response) => {
            skt.emit('tweet', response);
          });
        });
      });
    });
  });

  twitEmitter.on('tuser', (data) => {
    skt.emit('tuser', data);
  });

  twitEmitter.on('tweet', (data) => {
    skt.emit('tweet', data);
  });

  twitEmitter.on('test', (data) => {
    console.log(data);
  });
  twitEmitter.on('testo', (data) => {
    console.log(data);
  });

};