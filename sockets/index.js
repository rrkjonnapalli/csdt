'use strict';
const skt = require('./skt');
const logger = require('../utils/logger');
module.exports = (io) => {
  io.on('connection', (socket) => {
    if (!socket.handshake.session.passport || !socket.handshake.session.passport.user) {
      logger.debug('Invalid socket connection attempt. Disconnecting the user.');
      socket.disconnect();
      return;
    }
    logger.debug('Connection established.', socket.handshake.session.passport.user);
    skt(socket, io);
    io.on('disconnect', () => {
      console.log('Disconnected');
    });
  });
};